package com.codejava.main;

//import org.springframework.stereotype.Controller;
import com.codejava.main.models.AuthenticationRequest;
import com.codejava.main.models.AuthenticationResponse;
import com.codejava.main.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloResources {

    @Autowired
    private AuthenticationManager authenticationManager;
    
    @Autowired
    private UserDetailsService userDetailsService;
    
    @Autowired
    private JwtUtil jwtTokenUtil;
    
    
    @GetMapping("/hello")
   // @RequestMapping({"/hello"})
    public String hello(){
    
        //return "Hello JWT";
        return("<h1>Welcome to JWT</h1>");
        
    }
    
    @PostMapping("/authenticate" )
    public ResponseEntity<?> createAuthenticationToken(@RequestBody AuthenticationRequest authenticationRequest) throws Exception{
       try{
        authenticationManager.authenticate(
        new UsernamePasswordAuthenticationToken(authenticationRequest.getUsername(),authenticationRequest.getPassword())
        );
       }catch(BadCredentialsException e){
       
           throw new Exception("Incorrect Username or Password", e);
       }
       
       final UserDetails userDetails=userDetailsService
               .loadUserByUsername(authenticationRequest.getUsername());
       
       final String jwt =jwtTokenUtil.generateToken(userDetails);
       
        return ResponseEntity.ok(new AuthenticationResponse(jwt));
       
       
    }
     
}
